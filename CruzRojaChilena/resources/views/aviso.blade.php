@extends('layouts.app')
 <!-- Bootstrap core CSS-->
 <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/sistemalaravel.css">
@section('content')
<div class="container">
    <img class="inicio" src="imagenes/logo-cr.png">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-login mx-auto mt-6">
                
                <div class="card-body" style="align-items: center;  display: flex; justify-content: center;">
                    Lo sentimos, aun no estas ingresado efectivamente como un voluntario de la Cruz Roja, espera a que nuestro administrador revise tus datos.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>