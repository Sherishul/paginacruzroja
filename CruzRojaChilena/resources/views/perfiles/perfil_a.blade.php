@extends('layouts.app')
<?php
use App\Region;
?>
@section('content')
<div class="container">
                  <div class="row">
                      <div class="panel panel-default">
                      <div class="panel-heading">  <h2 >Perfil</h2></div>
                       <div class="panel-body">
                      <div class="col-md-16 col-xs-12 col-sm-12 " >
                          <div class="container" >
                            <h2>{{$voluntario->name}} {{$voluntario->lastname}}</h2>
                            

                            

                          </div>
                           <hr>
                            <ul class="container details" >
                            <li><p><span class="" style="width:50px;"></span>{{$voluntario->rut}}</p></li>
                              <li><p><span class="glyphicon glyphicon-envelope one" style="width:50px;"></span><a href="mailto:{{$voluntario->email}}">{{$voluntario->email}}</a></p></li>
                              <li><p><span class="fa fa-phone-square" style="width:50px;"></span>Fecha Nacimiento: {{$voluntario->fecha_nacimiento}}</p></li>
                                      <li><p><span class="fa fa-compass" style="width:50px;"></span>{{Region::find($voluntario->region)->nombre}}</p></li>

                              <li><p><span class="fa fa-building" style="width:50px;"></span>{{$voluntario->estado_civil}}</p></li>
                              <li><p><span class="fa fa-bar-chart" style="width:50px;"></span>Numero Registro: {{$voluntario->numero_registro}}</p></li>
                                      <li><p><span class="fa fa-industry" style="width:50px;"></span>Fecha Ingreso: {{$voluntario->fecha_ingreso}}</p></li>
                                      <li><p><span class="fa fa-bar-chart" style="width:50px;"></span>Direccion: {{$voluntario->direccion}}</p></li>
                                      <li><p><span class="fa fa-bar-chart" style="width:50px;"></span>Curso Basico: {{$voluntario->curso_basico}}</p></li>




                            </ul>
                    <form method="get" action="editar_voluntario" class="editar_voluntario">
                          <input type="hidden" name="id_voluntario" value="<?=$voluntario->id;?>">
                         <button class="btn btn-success " type="submit">Editar Voluntario</button>
                   </form>
                   <form method="get" action="eliminar_voluntario" class="eliminar_voluntario">
                      <input type="hidden" name="id_voluntario" value="<?=$voluntario->id;?>">
                        <button class="btn btn-danger " type="submit">Eliminar Voluntario</button>

                  </form>
                        
                </div>
            </div>
            </div>
</div>
</div>
@endsection