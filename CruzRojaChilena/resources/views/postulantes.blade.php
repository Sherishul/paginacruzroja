@extends('layouts.app')
<?php
    use App\Region;
?>
 <!-- Bootstrap core CSS-->
 <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/sistemalaravel.css">
@section('content')


        
             <h2><strong>Informacion de Postulantes</strong></h2>
    
            @foreach($usuarios as $usuario)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5><strong>{{$usuario->name}} {{$usuario->lastname}}</strong></h5>
                </div>
                    <div class="panel-body">
                    <h5 class="help-block"><strong>Email:                </strong> <span>{{$usuario->email}} </span></h5>
                    <h5 class="help-block"><strong>Rut:                  </strong> <span>{{$usuario->rut}} </span></h5>
                    <h5 class="help-block"><strong>Region:               </strong> <span>{{Region::find($usuario->region)->nombre}} </span></h5>
                    <h5 class="help-block"><strong>Fecha de Nacimiento:  </strong> <span>{{$usuario->fecha_nacimiento}} </span></h5>
                    <h5 class="help-block"><strong>Estado Civil:         </strong> <span>{{$usuario->estado_civil}} </span></h5>
                    <h5 class="help-block"><strong>Direccion:            </strong> <span>{{$usuario->direccion}} </span></h5>
                
                    
                    <form  id="f_aceptar_voluntario"  method="get"  action="aceptar_voluntario" class="aceptar_voluntario">
                    <input type="hidden" name="id_usuario" value="<?=$usuario->id;?>">
                        <h5 class="help-block"><strong>Ingresar Numero de Registro:</strong> <input type="number" class="form-control" name="numero">
                     
                        <button class="btn btn-primary" type="submit">Aceptar Voluntario</button>
                    
                    </form>

                    </div>
            </div>
            @endforeach
       

@endsection
