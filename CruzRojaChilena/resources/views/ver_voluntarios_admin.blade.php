@extends('layouts.app')

@section('content')

<a class="btn btn-primary pull-right" href="crear_voluntario">Crear Nuevo Voluntario </a>
<div class="box box-default box-solid col-xs-12">


                  <h2 class="box-title"><strong>Voluntarios</strong></h2>


<div class="box-footer">

  </div>


<div class="box-body">

<?php

if (count($voluntarios) > 0) {

    foreach ($voluntarios as $voluntario) {
        ?>
<?php if ($voluntario->rut != \Auth::user()->rut) {
            ?>

        <div class="col-xs-4 ">
            <div class="well well-sm col-sm-3 col-md-6">
                    <div class="col-md-2" style='width:120;height:120px;position:relative;'>
                      @if($voluntario->foto=="")
                        <img src="imagenes/cruz.png" style="width:100%;height:100%;position:absolute;"  class="img-rounded img-responsive" alt="User Avatar"/>
                      @else
                        <img src="{{$voluntario->foto}}" style="width:100%;height:100%;position:absolute;"  class="img-rounded img-responsive" alt="User Avatar"/>
                      @endif

                    </div>
                    <br />
                    <br />
                     
              
                    <div class="col-sm-6 col-md-8">           
                        <!-- Split button -->
                        <div class="col-sm-4">
                            <form method="get" action="cargar_perfil_a" name="cargar_perfil_a" >
                              <input type="hidden" name="id_voluntario" value="<?=$voluntario->id;?>">
                                <button class="btn btn-block btn-primary" style="text-overflow:ellipsis;white-space:nowrap;overflow:hidden;" type="submit">{{$voluntario->name}} {{$voluntario->lastname}}</button>
                            </form>
                        </div>
                </div>
            </div>
        </div>

<?php
}
    }
    ?>
<div class="box-body">
</div>




    </table>



    <?php


} else {

    ?>


<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun usuario...</label>  </div>

<?php
}

?>
</div>


@endsection