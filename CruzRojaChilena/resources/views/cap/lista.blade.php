@extends('layouts.app')
<?php
    use App\Region;
?>
 <!-- Bootstrap core CSS-->
 <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/sistemalaravel.css">
@section('content')
    @if($msj != "")
    <h3>{{$msj}}</h3>
    @endif
    <form method="get" action="ver_solicitudes" class="ver_solicitudes">
        <input type="hidden" name="id_voluntario" value="<?=$voluntario->id;?>">
    <button class="btn btn-primary pull-right" type="submit">Ver Solicitudes Actuales</button>

    </form>
          
             <h2><strong>Informacion de Capacitaciones</strong></h2>
             @foreach($capacitaciones as $capacitacion)
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5><strong>{{$capacitacion->tema}}</strong></h5>
                    </div>
                        <div class="panel-body">
                        <h5 class="help-block"><strong>Descripcion:                </strong> <span>{{$capacitacion->descripcion}} </span></h5>
                        <h5 class="help-block"><strong>Encargados:                  </strong> <span>{{$capacitacion->encargados}} </span></h5>
                        <h5 class="help-block"><strong>Lugar:               </strong> <span>{{$capacitacion->lugar}} </span></h5>
                        <h5 class="help-block"><strong>Duracion:  </strong> <span>{{$capacitacion->duracion}} </span></h5>
                        <h5 class="help-block"><strong>Fecha Inicio:         </strong> <span>{{$capacitacion->fecha_inicio}} </span></h5>
                        <h5 class="help-block"><strong>Fecha Termino:            </strong> <span>{{$capacitacion->fecha_termino}} </span></h5>
                        <h5 class="help-block"><strong>Cupo Actual:            </strong> <span>{{$capacitacion->cupo}} </span></h5>
                        <h5 class="help-block"><strong>Inscritos:            </strong> <span>{{$capacitacion->inscritos}} </span></h5>
                        <h5 class="help-block"><strong>Requisitos:            </strong> <span>{{$capacitacion->requisitos}} </span></h5>
                      
                        <form  id="f_inscribirse"  method="get"  action="soli_inscribirse" class="soli_inscribirse">
                    <input type="hidden" name="id_voluntario" value="<?=$voluntario->id;?>">     
                    <input type="hidden" name="id_capacitacion" value="<?=$capacitacion->id;?>">
                        <button class="btn btn-primary" type="submit">Solicitar Inscripcion</button>
                    
                    </form>

                        </div>
            </div>
    </div>
</div>
</div>
    @endforeach
            
                
          
       

@endsection
