@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">    
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Crear Capacitacion') }}  </div>
                 
                <div class="card-body">
                    <form method="POST" action="generar_capacitacion" id="f_generar_capacitacion" class="generar_capacitacion">
                        @csrf

                        <div class="form-group row">
                            <label for="tema" class="col-md-4 col-form-label text-md-right">{{ __('Tema') }}</label>

                            <div class="col-md-6">
                                <input id="tema" type="text" class="form-control{{ $errors->has('tema') ? ' is-invalid' : '' }}" name="tema" value="{{ old('tema') }}" required autofocus>

                            
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">Descripcion</label>

                            <div class="col-md-6">
                                <input id="descripcion" type="text" class="form-control" name="descripcion" value="{{ old('descripcion') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="encargados" class="col-md-4 col-form-label text-md-right">{{ __('Encargados') }}</label>

                            <div class="col-md-6">
                                <input id="encargados" type="encargados" class="form-control{{ $errors->has('encargados') ? ' is-invalid' : '' }}" name="encargados" value="{{ old('encargados') }}" required>

                              
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lugar" class="col-md-4 col-form-label text-md-right">{{ __('Lugar') }}</label>

                            <div class="col-md-6">
                                <input id="lugar" type="lugar" class="form-control{{ $errors->has('lugar') ? ' is-invalid' : '' }}" name="lugar" required>

                               
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="duracion" class="col-md-4 col-form-label text-md-right">{{ __('Duracion') }}</label>

                            <div class="col-md-6">
                                <input id="duracion" type="lugar" class="form-control" name="duracion" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_inicio" class="col-md-4 col-form-label text-md-right">{{ __('Fecha Inicio') }}</label>

                            <div class="col-md-6">
                                <input id="fecha_inicio" type="fecha_inicio" class="form-control" name="fecha_inicio" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_termino" class="col-md-4 col-form-label text-md-right">{{ __('Fecha Termino') }}</label>

                            <div class="col-md-6">
                                <input id="fecha_termino" type="fecha_termino" class="form-control" name="fecha_termino" required>
                            </div>
                        </div>      

                        <div class="form-group row">
                            <label for="cupo" class="col-md-4 col-form-label text-md-right">{{ __('Cupo') }}</label>

                            <div class="col-md-6">
                                <input id="cupo" type="cupo" class="form-control" name="cupo" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="requisitos" class="col-md-4 col-form-label text-md-right">{{ __('Requisitos') }}</label>

                            <div class="col-md-6">
                                <input id="requisitos" type="requisitos" class="form-control" name="requisitos" required>
                            </div>
                        </div>

                        <div class="form-group row ">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Crear Nueva Capacitacion') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
