@extends('layouts.app')
<?php
    use App\Region;
?>
 <!-- Bootstrap core CSS-->
 <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/sistemalaravel.css">
@section('content')

<div class="panel panel-default">
    <div class="panel-header"><h2>Capacitaciones </h2></div> 
    <div class="panel-body">
        <a class="btn btn-primary" href="crear_capacitacion" >Crear Capacitacion</a>
        <a class="btn btn-primary" href="revisar_solicitudes">Revisar Solicitudes</a>
        @foreach($capacitaciones as $capacitacion)
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5><strong>{{$capacitacion->tema}}</strong></h5>
                    </div>
                        <div class="panel-body">
                        <h5 class="help-block"><strong>Descripcion:                </strong> <span>{{$capacitacion->descripcion}} </span></h5>
                        <h5 class="help-block"><strong>Encargados:                  </strong> <span>{{$capacitacion->encargados}} </span></h5>
                        <h5 class="help-block"><strong>Lugar:               </strong> <span>{{$capacitacion->lugar}} </span></h5>
                        <h5 class="help-block"><strong>Duracion:  </strong> <span>{{$capacitacion->duracion}} </span></h5>
                        <h5 class="help-block"><strong>Fecha Inicio:         </strong> <span>{{$capacitacion->fecha_inicio}} </span></h5>
                        <h5 class="help-block"><strong>Fecha Termino:            </strong> <span>{{$capacitacion->fecha_termino}} </span></h5>
                        <h5 class="help-block"><strong>Cupo Actual:            </strong> <span>{{$capacitacion->cupo}} </span></h5>
                        <h5 class="help-block"><strong>Inscritos:            </strong> <span>{{$capacitacion->inscritos}} </span></h5>
                        <h5 class="help-block"><strong>Requisitos:            </strong> <span>{{$capacitacion->requisitos}} </span></h5>
                        <form method="get" action="ver_inscritos" class="ver_inscritos">
                          <input type="hidden" name="id_capacitacion" value="<?=$capacitacion->id;?>">
                         <button class="btn btn-info " type="submit">Ver Inscritos</button>
                   </form>
                        </div>
            </div>
    </div>
</div>
</div>
    @endforeach
       

@endsection
