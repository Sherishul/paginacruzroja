@extends('layouts.app')
<?php
    use App\Voluntario;
    use App\Capacitacion;
?>
 <!-- Bootstrap core CSS-->
 <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/sistemalaravel.css">
@section('content')

             <h2><strong>Informacion de Solicitudes</strong></h2>
             @foreach($enlace as $enlacito)
             <?php
                    $capacitacion = Capacitacion::all()->where('id',$enlacito->id_capacitacion)->first();
                    $voluntario = Voluntario::all()->where('id',$enlacito->id_voluntario)->first();
             ?>
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5><strong>{{$capacitacion->tema}}</strong></h5>
                    </div>
                        <div class="panel-body">
                        <h5 class="help-block"><strong>Nombre Voluntario:                </strong> <span>{{$voluntario->name}}  {{$voluntario->lastname}} </span></h5>
                        <h5 class="help-block"><strong>Requisitos Capacitacion:                </strong> <span>{{$capacitacion->requisitos}} </span></h5>
                        <h5 class="help-block"><strong>Cursos Voluntario:                </strong> <span>{{$voluntario->curso_basico}} </span></h5>
                        <h5 class="help-block"><strong>Cupo Capacitacion:                </strong> <span>{{$capacitacion->cupo}} </span></h5>
                        </div>  

                      <form method="get" action="aceptar_solicitud" class="aceptar_solicitud">
                          <input type="hidden" name="id_enlace" value="<?=$enlacito->id;?>">
                         <button class="btn btn-success " type="submit">Aceptar Solicitud</button>
                   </form>
                   <form method="get" action="rechazar_solicitud" class="rechazar_solicitud">
                      <input type="hidden" name="id_enlace" value="<?=$enlacito->id;?>">
                        <button class="btn btn-danger " type="submit">Rechazar Solicitud</button>

                  </form>
            </div>
    </div>
</div>
</div>
    @endforeach
            
                
          
       

@endsection
