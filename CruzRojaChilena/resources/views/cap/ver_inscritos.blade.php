@extends('layouts.app')
<?php
    use App\Voluntario;
?>
 <!-- Bootstrap core CSS-->
 <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/sistemalaravel.css">
@section('content')

             <h2><strong>{{$capacitacion->tema}}</strong></h2>
             <h3>Inscritos</h3>
             @foreach($enlaces as $enlace)
             <?php
                    $voluntario = Voluntario::all()->where('id',$enlace->id_voluntario)->first();
             ?>
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5><strong></strong></h5>
                    </div>
                        <div class="panel-body">
                        <h5 class="help-block"><strong>Nombre:                </strong> <span>{{$voluntario->name}} {{$voluntario->last_name}} </span></h5>
                        <h5 class="help-block"><strong>Fecha Inscripcion:                </strong> <span>{{$enlace->updated_at}} </span></h5>
                        </div>
            </div>
    </div>
</div>
</div>
    @endforeach

@endsection
            
                
          
       
