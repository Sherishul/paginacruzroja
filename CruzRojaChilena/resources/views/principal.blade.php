@extends('layouts.app')
<!doctype html>
<html lang="en">
@section('content')
<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
</head>
<body>
    <div class="welcome">
        <h1>Bienvenido {{ Auth::user()->name }}</h1>
        <a class="btn btn-primary"href="ver_capacitaciones">Capacitaciones Disponibles</a>
    </div>
</body>
</html>
@endsection
