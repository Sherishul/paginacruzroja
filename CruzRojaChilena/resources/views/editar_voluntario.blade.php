@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">    
    <img class="inicio" src="imagenes/logo-cr.png">
        <div class="col-md-8">
        @if($msj != "")
            <h2>{{$msj}}</h2>
        @endif
                    <div class="card">
                <div class="card-header">{{ __('Editar Voluntario') }}</div>

                <div class="card-body">
                    <form method="POST" action="guardar_voluntario">
                        @csrf
                        <input type="hidden" name="id_voluntario" value="<?=$voluntario->id;?>">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $voluntario->name }}" required autofocus>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">Apellido</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ $voluntario->lastname }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$voluntario->email}}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="estadoc" class="col-md-4 col-form-label text-md-right">{{ __('Estado Civil') }}</label>

                            <div class="col-md-6">
                                <input id="estadoc" type="estadoc" class="form-control" name="estadoc" value="{{$voluntario->estado_civil}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                        <label for="region"  class="col-md-4 col-form-label text-md-right">Region</label>
                             <div class="col-md-6">
                          
                              <select id="region" name="region" class="form-control">
                                    @foreach($regiones as $region)
                                      <option value="<?=$region->id?>" @if($region->id == $voluntario->region) selected @endif >{{$region->nombre}}</option>
                                    @endforeach

                            </select>
                            </div>   
                            </div>


                        <div class="form-group row">
                            <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>

                            <div class="col-md-6">
                                <input id="direccion" type="direccion" class="form-control" name="direccion" value="{{$voluntario->direccion}}" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Guardar Voluntario') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
