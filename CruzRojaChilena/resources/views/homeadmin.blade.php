@extends('layouts.app')
<?php
    use App\Region;
?>
 <!-- Bootstrap core CSS-->
 <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/sistemalaravel.css">
@section('content')

<div class="panel panel-default">
    <div class="panel-header"><h2>Bienvenido {{$nuevo->name}} {{$nuevo->lastname}} </h2></div> 
    <div class="panel-body">
        <a class="btn btn-primary" href="postulantes" >Ver Postulantes</a>
        <a class="btn btn-primary" href="capacitaciones_a">Ver/Crear Capacitaciones </a>
        <a class="btn btn-primary" href="ver_voluntarios">Ver Voluntarios </a>
    </div>
</div>
    
       

@endsection
