@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">    
    <img class="inicio" src="imagenes/logo-cr.png">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registrarse') }}</div>

                <div class="card-body">
                    <form method="POST" action="registrar">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">Apellido</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="rut" class="col-md-4 col-form-label text-md-right">{{ __('Rut') }}</label>

                            <div class="col-md-6">
                                <input id="rut" type="rut" class="form-control" name="rut" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="estadoc" class="col-md-4 col-form-label text-md-right">{{ __('Estado Civil') }}</label>

                            <div class="col-md-6">
                                <input id="estadoc" type="estadoc" class="form-control" name="estadoc" required>
                            </div>
                        </div>

                        <div class="form-group row">
                        <label for="region"  class="col-md-4 col-form-label text-md-right">Region</label>
                             <div class="col-md-6">
                          
                              <select id="region" name="region" class="form-control">
                                      <option value="1">Ninguna</option>
                                      <option value="2">Todas</option>
                                      <option value="3">Arica y Parinacota</option>
                                      <option value="4">Tarapaca</option>
                                      <option value="5">Antofagasta</option>
                                      <option value="6">Atacama</option>
                                      <option value="7">Coquimbo</option>
                                      <option value="8">Valparaiso</option>
                                      <option value="9">Region Metropolitana</option>
                                      <option value="10">Libertador General Bernardo O'Higgins</option>
                                      <option value="11">Maule</option>
                                      <option value="12">Bio-Bio</option>
                                      <option value="13">Araucania</option>
                                      <option value="14">Los Rios</option>
                                      <option value="15">Los Lagos</option>
                                      <option value="16">Aysen</option>
                                      <option value="17">Magallanes</option>

                            </select>
                            </div>   
                            </div>
                        

                        <div class="form-group row">
                            <label for="nacimiento" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Nacimiento') }}</label>

                            <div class="col-md-6">
                                <input id="nacimiento" type="nacimiento" class="form-control" name="nacimiento" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>

                            <div class="col-md-6">
                                <input id="direccion" type="direccion" class="form-control" name="direccion" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrarse') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
