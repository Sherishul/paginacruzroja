@extends('layouts.app')
<head>
  <link rel="icon" href="imagenes/ico.png">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Inicio de Sesión</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link rel="stylesheet" href="css/sistemalaravel.css">
</head>
@section('content')
<body>

    <div class="container">
    <img class="inicio" src="imagenes/logo-cr.png">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header"> Bienvenido a la Plataforma! </div>
                <div class="card-body"> Bienvenido, {{$usuario->name}} {{$usuario->lastname}} para continuar con el registro debera esperar que el director lo verifique como voluntario.
                    <div class="text-center" >
                        <form action="principal" method="get">
                              <input type="hidden" name="id_usuario" value="<?=$usuario->id;?>">
                             <button class="btn btn-primary btn-block">Ir a Pagina Principal</button>
                        </form>
                    </div>
                 </div> 
          </div>
    </div>
</body>
@endsection