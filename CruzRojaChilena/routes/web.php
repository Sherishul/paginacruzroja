<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('registrar', 'Auth\AuthController@postRegister');
Route::get('recoverpass', 'Auth\ForgotPasswordController@showLinkRequestForm');

Route::get('/', 'HomeController@index1');
Route::get('', 'HomeController@index1');

Route::get('principal','PrincipalController@paginaPrincipal'); //redireccion a voluntarios
Route::get('aviso','PrincipalController@paginaPrincipal'); // Redireccion para ver que aun no es voluntario

Route::get('aceptar_voluntario','VoluntarioController@aceptarVoluntario');
Route::get('postulantes','VoluntarioController@verPostulantes');
Route::get('capacitaciones_a','VoluntarioController@capacitacionesAdmin');
Route::get('crear_capacitacion','CapacitacionController@crearCapacitacion');  
Route::post('generar_capacitacion','CapacitacionController@generarCapacitacion');
Route::get('ver_capacitaciones','CapacitacionController@listaCapacitaciones');
Route::get('soli_inscribirse','CapacitacionController@solicitudCapacitacion');
Route::get('ver_solicitudes','CapacitacionController@verSolicitudes');
Route::get('revisar_solicitudes','CapacitacionController@revisarSolicitudes');
Route::get('aceptar_solicitud','CapacitacionController@aceptarSolicitud');
Route::get('rechazar_solicitud','CapacitacionController@rechazarSolicitud');
Route::get('ver_inscritos','CapacitacionController@verInscritos');
Route::get('ver_voluntarios','VoluntarioController@verVoluntarios');
Route::get('crear_voluntario','VoluntarioController@crearVoluntario');
Route::post('crear_n_voluntario','VoluntarioController@crearNuevoVoluntario');
Route::get('cargar_perfil_a','VoluntarioController@perfilVoluntarioA');
Route::get('editar_voluntario','VoluntarioController@editarVoluntario');
Route::get('eliminar_voluntario','VoluntarioController@eliminarVoluntario');
Route::post('guardar_voluntario','VoluntarioController@guardarVoluntario');