<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voluntario;
use App\User;
use App\Filial;
use App\Capacitacion;
use App\Voluntario_Capacitacion;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Auth;
use Session;

class CapacitacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function crearCapacitacion(){
        return view('cap.crear_capacitacion');
    }

    public function generarCapacitacion(Request $request){
        $capacitacion = new Capacitacion;

        $capacitacion->tema = $request['tema'];
        $capacitacion->descripcion = $request['descripcion'];
        $capacitacion->encargados = $request['encargados'];
        $capacitacion->lugar = $request['lugar'];
        $capacitacion->duracion = $request['duracion'];
        $capacitacion->fecha_inicio = $request['fecha_inicio'];
        $capacitacion->fecha_termino = $request['fecha_termino'];
        $capacitacion->cupo = $request['cupo'];
        $capacitacion->inscritos = 0;
        $capacitacion->requisitos = $request['requisitos'];

        $capacitacion->save();
        $capacitaciones = Capacitacion::all();
        
        return view('cap.admin',compact ('capacitaciones'));
    }

    public function listaCapacitaciones(){
        $capacitaciones = Capacitacion::all();
        $usuario = \Auth::user();
        $voluntario = Voluntario::all()->where('rut',$usuario->rut)->first();
        $msj= "";
        return view ('cap.lista',compact('capacitaciones','voluntario','msj'));
    }

    public function solicitudCapacitacion(Request $request){
        $prueba = Voluntario_Capacitacion::all()->where('id_voluntario',$request['id_voluntario'])->where('id_capacitacion',$request['id_capacitacion'])->first();
        if($prueba == []){
            $enlace = new Voluntario_Capacitacion;
            $enlace->id_voluntario= $request['id_voluntario'];
            $enlace->id_capacitacion = $request['id_capacitacion'];
            $enlace->estado = 'solicitado';
            $enlace->save();


        $capacitaciones = Capacitacion::all();
        $voluntario = Voluntario::find($request['id_voluntario']);
        $msj = "Se ha Ingresado su solicitud";
        return view('cap.lista',compact('capacitaciones','voluntario','msj'));
        }else{
            $capacitaciones = Capacitacion::all();
            $voluntario = Voluntario::find($request['id_voluntario']);
            $msj= "Se ha encontrado una solicitud vigente aun";
            return view('cap.lista',compact('capacitaciones','voluntario','msj'));
        }
    }

    public function verSolicitudes(Request $request){
        $enlace = Voluntario_Capacitacion::all()->where('id_voluntario',$request['id_voluntario']);
        return view('cap.ver_solicitudes',compact('enlace'));
    
    }

    public function revisarSolicitudes(){
        $enlace =Voluntario_Capacitacion::all()->where('estado','solicitado');
        return view('cap.revision_solicitudes',compact('enlace'));
    }

    public function rechazarSolicitud(Request $request){
        $enlace2 =Voluntario_Capacitacion::all()->where('id',$request['id_enlace'])->first();
        $enlace2->estado = 'Rechazado';
        $enlace2->save();

        $enlace =Voluntario_Capacitacion::all()->where('estado','solicitado');
        return view('cap.revision_solicitudes',compact('enlace'));
    }

    public function aceptarSolicitud(Request $request){
        $enlace2 = Voluntario_Capacitacion::all()->where('id',$request['id_enlace'])->first();
        $enlace2->estado = 'Aceptado';
        $enlace2->save();

        $capacitacion = Capacitacion::find($enlace2->id_capacitacion);
        $capacitacion->cupo--;
        $capacitacion->inscritos++;
        $capacitacion->save();

        $enlace =Voluntario_Capacitacion::all()->where('estado','solicitado');
        return view('cap.revision_solicitudes',compact('enlace'));
    }

    public function verInscritos(Request $request){
        $enlaces = Voluntario_Capacitacion::all()->where('id_capacitacion',$request['id_capacitacion'])->where('estado','Aceptado');
        $capacitacion = Capacitacion::find($request['id_capacitacion']);
        return view('cap.ver_inscritos',compact('enlaces','capacitacion'));
    }
}
