<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voluntario;
use App\User;
use App\Filial;
use App\Region;
use App\Capacitacion;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Auth;
use Session;

class VoluntarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function aceptarVoluntario(Request $request){
        $usuario = User::find($request['id_usuario']);

        $voluntario = new Voluntario;

        $voluntario->name  = $usuario->name;
        $voluntario->lastname = $usuario->lastname;
        $voluntario->email = $usuario->email;
        $voluntario->rut = $usuario->rut;
        $voluntario->region = $usuario->region;
        $voluntario->fecha_nacimiento = $usuario->fecha_nacimiento;
        $voluntario->estado_civil = $usuario->estado_civil;
        $voluntario->numero_registro = $request['numero'];
        $voluntario->fecha_ingreso = $usuario->fecha_ingreso;
        $voluntario->direccion = $usuario->direccion;
        $voluntario->curso_basico = 'No';

        $voluntario->save();

        //enviar correo al voluntario para que sepa que es gay ahora

        $nuevo = \Auth::user();
        $usuarios = User::all();
        return view('homeadmin',compact('nuevo','usuarios'));
    }

    public function verPostulantes(){
        $nuevo = \Auth::user();
        $usuarios = User::all();

        return view('postulantes',compact('nuevo','usuarios'));
    }

    public function capacitacionesAdmin(){
        $capacitaciones = Capacitacion::all();

        return view('cap.admin',compact('capacitaciones'));
    }

    public function verVoluntarios(){
        $voluntarios = Voluntario::all();

        return view('ver_voluntarios_admin',compact('voluntarios'));
    }

    public function crearVoluntario(){
        $msj="";
        return view('form_crear_voluntario',compact('msj'));
    }

    public function crearNuevoVoluntario(Request $request){


        $data    = $request;
        $usuarios = User::all()->where('rut', $data['rut']);
        if (!$usuarios->isEmpty()) {

            $msj       = "Voluntario ya existe en la base de datos";
            return view("form_crear_voluntario", compact('msj'));
        }
        if ($data['password'] != $data['password_confirmation']) {


            $msj       = "Las contraseñas no coinciden.";
            return view("form_crear_voluntario", compact('msj'));
        }
        if( !Rut::parse($data['rut'])->validate()){
            
            $msj       = "El rut ingresado no es valido.";
            return view("form_crear_voluntario", compact('msj'));
        }
        $usuario            = new User;
        $usuario->name      = $data['name'];
        $usuario->lastname = $data['lastname'];
        $usuario->email     = $data['email'];
        $usuario->rut        = Rut::parse($data['rut'])->normalize();
        $usuario->region        = $data['region'];
        $usuario->estado_civil          = $data['estadoc'];
        $usuario->fecha_nacimiento        = $data['nacimiento'];
        $usuario->direccion        = $data['direccion'];
        $usuario->fecha_ingreso = Carbon::now(); 
        $usuario->foto= '';
        

        $usuario->password = bcrypt($data['password']);
         
        $usuario->save();

        $voluntario = new Voluntario;
        $voluntario->name      = $data['name'];
        $voluntario->lastname = $data['lastname'];
        $voluntario->email     = $data['email'];
        $voluntario->rut        = Rut::parse($data['rut'])->normalize();
        $voluntario->region        = $data['region'];
        $voluntario->estado_civil          = $data['estadoc'];
        $voluntario->fecha_nacimiento        = $data['nacimiento'];
        $voluntario->direccion        = $data['direccion'];
        $voluntario->fecha_ingreso = Carbon::now(); 
        $voluntario->foto= '';
        $voluntario->numero_registro = $data['num_re'];
        $voluntario->save();
    }

    public function perfilVoluntarioA(Request $request){
        $voluntario = Voluntario::find($request['id_voluntario']);
        return view('perfiles.perfil_a',compact('voluntario'));
    }

    public function editarVoluntario(Request $request){
        $voluntario = Voluntario::find($request['id_voluntario']);
        $msj='';
        $regiones = Region::all();
        return view('editar_voluntario',compact('voluntario','msj','regiones'));
    }

    public function eliminarVoluntario(Request $request){
        $voluntario = Voluntario::find($request['id_voluntario']);
        $voluntario->delete();

        $voluntarios = Voluntario::all();
        return view('ver_voluntarios_admin',compact('voluntarios'));
    }

    public function guardarVoluntario(Request $request){
        $voluntario = Voluntario::find($request['id_voluntario']);

        $voluntario->name   = $request['name'];
        $voluntario->lastname   = $request['lastname'];
        $voluntario->email  = $request['email'];
        $voluntario->estado_civil   = $request['estadoc'];
        $voluntario->region = $request['region'];
        $voluntario->direccion  = $request['direccion'];
        $voluntario->save();
        
        return view('perfiles.perfil_a',compact('voluntario'));
    }
}
