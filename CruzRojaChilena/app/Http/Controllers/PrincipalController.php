<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voluntario;
use App\User;
use App\Filial;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Auth;
use Session;

class PrincipalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function paginaPrincipal(Request $request){
        
        $nuevo= \Auth::user();

        $voluntario= Voluntario::all()->where('rut',$nuevo->rut)->first();
            if($voluntario != []){
               
                $filial = Filial::all()->where('voluntario',$voluntario->id)->first();
                if($filial != []){
               
                    if($filial->cargo == '1'){
                      
                    
                        return view('homeadmin', compact('nuevo'));
                    }
                    if($filial->cargo == '2'){
                        return view('homeadmin', compact('nuevo'));
                    }
                    if($filial->cargo == '3'){
                        return view('principal', compact('nuevo'));
                    }
                    if($filial->cargo == '4'){
                        return view('principal', compact('nuevo'));
                    }
                }else{
              return view('principal', compact('nuevo'));
                }
            }else{

            return view('aviso');
            }
    }


}
