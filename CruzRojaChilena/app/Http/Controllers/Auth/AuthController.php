<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Session;
use Validator;
use Mail;
use App\Region;
use App\User;
use App\Voluntario;
use App\Filial;
use Carbon\Carbon;
use Freshwork\ChileanBundle\Rut;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
     */

    //use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest', ['except' => 'getLogout']);

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

//login

    protected function getLogin()
    {
        $regiones = Region::all();

        return view('auth.login', compact('regiones'));
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'rut'    => 'required',
            'password' => 'required',
        ]);

        $credentials = array(
            'rut' => $request['rut'],
            'password'=> $request['password']
        );

        if (\Auth::attempt($credentials, $request->has('remember'))) {
           
            $nuevo = \Auth::user();
            $voluntario= Voluntario::all()->where('rut',$nuevo->rut)->first();
       
            if($voluntario != []){
                $filial = Filial::all()->where('voluntario',$voluntario->id)->first();
                if($filial != []){
                 
                    if($filial->cargo == '1'){
                       
                        

                        return view('homeadmin', compact('nuevo'));
                    }
                    if($filial->cargo == '2'){
                        return view('homeadmin', compact('nuevo'));
                    }
                    if($filial->cargo == '3'){
                        return view('principal', compact('nuevo'));
                    }
                    if($filial->cargo == '4'){
                        return view('principal', compact('nuevo'));
                    }
                }else{
              return view('principal', compact('nuevo'));
                }
            }else{
            return view('aviso');
            }
            
        }

        return view("auth.login")->with("msj", "Correo y/o contraseña incorrecta");

    }

//login

    //registro

    protected function getRegister()
    {
        $regiones   = Region::all();
       
        return view("auth.register", compact('regiones'));
    }

    protected function postRegister(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'email'    => 'required',
            'password' => 'required',
        ]);

        $data    = $request;
        $usuarios = User::all()->where('rut', $data['rut']);
        if (!$usuarios->isEmpty()) {

            $nom       = $data['name'];
            $ape       = $data['LastName'];
            $email     = $data['email'];
            $region    = $data['region'];
            $msj       = "El rut ingresado ya existe.";
            return view("auth.register2", compact('msj', 'nom', 'ape', 'email', 'region'));
        }
        if ($data['password'] != $data['password_confirmation']) {

            $nom       = $data['name'];
            $ape       = $data['LastName'];
            $email     = $data['email'];
            $region    = $data['region'];
            $msj       = "Las contraseñas no coinciden.";
            return view("auth.register2", compact('msj', 'nom', 'ape', 'email', 'region'));
        }
        if( !Rut::parse($data['rut'])->validate()){
            
            $nom       = $data['name'];
            $ape       = $data['LastName'];
            $email     = $data['email'];
            $region    = $data['region'];
            $msj       = "El rut ingresado no es valido.";
            return view("auth.register2", compact('msj', 'nom', 'ape', 'email', 'region'));
        }
        $usuario            = new User;
        $usuario->name      = $data['name'];
        $usuario->lastname = $data['lastname'];
        $usuario->email     = $data['email'];
        $usuario->rut        = Rut::parse($data['rut'])->normalize();
        $usuario->region        = $data['region'];
        $usuario->estado_civil          = $data['estadoc'];
        $usuario->fecha_nacimiento        = $data['nacimiento'];
        $usuario->direccion        = $data['direccion'];
        $usuario->fecha_ingreso = Carbon::now(); 
        $usuario->foto= '';
        

        $usuario->password = bcrypt($data['password']);
         
        $usuario->save();
    //    Mail::send('holabb', $arreglo, function ($msj) use ($usuario) {

    //            $msj->subject('Bienvenido a la Plataforma de Cruz Roja');
    //            $msj->to($usuario->email, $usuario->name);
    //        });

            return view('auth.bienvenido', compact ('usuario'));
    }

//registro

    protected function getLogout()
    {
        $this->auth->logout();

        Session::flush();

        return redirect('/');
    }


}
