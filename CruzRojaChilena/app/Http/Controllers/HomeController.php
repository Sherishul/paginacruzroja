<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Validator;
use Mail;
use App\Region;
use App\User;
use Carbon\Carbon;
use Freshwork\ChileanBundle\Rut;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('principal');
    }

    public function index1(){

        if (\Auth::check())
        {
            
            return redirect('principal');
        }
        return view('auth.login');
    }
}
