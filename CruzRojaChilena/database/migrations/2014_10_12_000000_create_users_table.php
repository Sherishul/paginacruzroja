<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname')->default("");
            $table->string('email', 100);
            $table->string('password', 60);
            $table->string('rut');
            $table->string('region');
            $table->string('fecha_nacimiento');
            $table->string('estado_civil');
            $table->string('fecha_ingreso');
            $table->string('direccion');
            $table->string('foto')->default("");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
