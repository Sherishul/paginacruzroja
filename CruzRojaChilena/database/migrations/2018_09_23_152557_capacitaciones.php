<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Capacitaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacitaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tema');
            $table->string('descripcion');
            $table->string('encargados');
            $table->string('lugar');
            $table->string('duracion');
            $table->string('fecha_inicio');
            $table->string('fecha_termino');
            $table->integer('cupo');
            $table->integer('inscritos');
            $table->string('requisitos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
