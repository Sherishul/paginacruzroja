<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Voluntarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voluntarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname')->default("");
            $table->string('email');
            $table->string('rut');
            $table->string('region');
            $table->string('fecha_nacimiento');
            $table->string('estado_civil');
            $table->string('numero_registro');
            $table->string('fecha_ingreso');
            $table->string('direccion');
            $table->string('curso_basico');
            $table->string('foto')->default("");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
